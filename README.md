统一开发 统一开放 统一规范 统一开源
http://git.oschina.net/jehc/jehc

交流群：635727104（我的），330370132（项目作者的）

效果图片如下：
首页
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/174338_5ba828c1_1341290.png "在这里输入图片标题")
公司信息
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/174452_7e335b8d_1341290.png "在这里输入图片标题")
部门信息
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/174533_9d389d04_1341290.png "在这里输入图片标题")
岗位信息
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/174617_0c9279c1_1341290.png "在这里输入图片标题")
组织机构
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/174726_ea938b1e_1341290.png "在这里输入图片标题")
数据权限
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/174836_fd84c307_1341290.png "在这里输入图片标题")
角色权限
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/174924_a90ce96e_1341290.png "在这里输入图片标题")
短消息
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/175014_3e574ee7_1341290.png "在这里输入图片标题")
曲线图
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/175055_aae4a845_1341290.png "在这里输入图片标题")
CPU监控
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/175138_ab65b686_1341290.png "在这里输入图片标题")
JVM监控
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/175217_373c11c4_1341290.png "在这里输入图片标题")
内存监控
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/175259_564d3efb_1341290.png "在这里输入图片标题")
载入页面监控
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/175355_4dad22d4_1341290.png "在这里输入图片标题")
业务平台操作日志
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/175445_c2d2d192_1341290.png "在这里输入图片标题")
修改记录监控
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/175547_d7eeae9c_1341290.png "在这里输入图片标题")
磁盘监控
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/175711_b7610aae_1341290.png "在这里输入图片标题")
动态调度器
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/175802_a4ab56f7_1341290.png "在这里输入图片标题")
solr全文检索
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/175846_1fa6e2c7_1341290.png "在这里输入图片标题")
缓存中心
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/175932_6cd4dc55_1341290.png "在这里输入图片标题")
配置中心
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/180019_298e6657_1341290.png "在这里输入图片标题")
activiti工作流设计器
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/180116_6f47fbdb_1341290.png "在这里输入图片标题")
代码生成器-单表
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/180238_d8ac2ebf_1341290.png "在这里输入图片标题")
代码生成器-多表
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/180325_6c611d9d_1341290.png "在这里输入图片标题")
sql查询器
![输入图片说明](https://git.oschina.net/uploads/images/2017/0626/180431_10421d19_1341290.png "在这里输入图片标题")

统一开发 统一开放 统一规范 统一开源
http://git.oschina.net/jehc/jehc

交流群：635727104（我的），330370132（项目作者的）

