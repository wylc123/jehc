package jehc.xtmodules.xtcore.util;
/**
 * 系统常量 流程可以不再使用该定义
 * @author 邓纯杰
 *
 */
public class SysContanst {
	//外出登记
	public static final String OaMattendOut = "OaMattendOut";
	public static final String OaMattendOut_URL = "oaMattendOut/oaMattendOut_loadOaMattendOutDetail.nzy";
	
	//请假登记
	public static final String OaMattendAfl = "OaMattendAfl";
	public static final String OaMattendAfl_URL = "oaMattendAfl/oaMattendAfl_loadOaMattendAflDetail.nzy";
}
